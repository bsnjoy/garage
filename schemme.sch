EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 26250 7900
Connection ~ 16750 8200
Connection ~ 18700 11700
Connection ~ 14350 5650
Connection ~ 650  -2750
$Comp
L R 10k
U 1 1 550A6154
P 3750 3600
F 0 "10k" V 3830 3600 50  0000 C CNN
F 1 "R" V 3750 3600 50  0000 C CNN
F 2 "" V 3680 3600 30  0000 C CNN
F 3 "" H 3750 3600 30  0000 C CNN
	1    3750 3600
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X13 Pi
U 1 1 550A61FE
P 2950 3650
F 0 "Pi" H 2950 4350 50  0000 C CNN
F 1 "CONN_02X13" V 2950 3650 50  0000 C CNN
F 2 "" H 2950 2500 60  0000 C CNN
F 3 "" H 2950 2500 60  0000 C CNN
	1    2950 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2700 2700 2700
Wire Wire Line
	2700 2700 2700 3050
$Comp
L SPST SW?
U 1 1 550A6A0D
P 5000 3750
F 0 "SW?" H 5000 3850 50  0000 C CNN
F 1 "SPST" H 5000 3650 50  0000 C CNN
F 2 "" H 5000 3750 21  0000 C CNN
F 3 "" H 5000 3750 21  0000 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3250 5500 3250
Wire Wire Line
	5500 3250 5500 3750
Wire Wire Line
	3200 3750 4500 3750
Connection ~ 3750 3750
Wire Wire Line
	3750 3450 3750 2700
Text Notes 3300 3250 0    60   ~ 0
GND
Text Notes 2650 2650 0    60   ~ 0
+3.3V
$EndSCHEMATC
