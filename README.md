# Управление гаражными воротами #

Основаная плата - Raspberry Pi 2 Model B (Revision: 000e) [Pinout](https://pinout.xyz)
Подсоединяется к [Relay Shield for Arduino V2.1 (SKU:DFR0144)](https://www.dfrobot.com/wiki/index.php/Relay_Shield_for_Arduino_V2.1_(SKU:DFR0144)) (Можно вместо него использовать 4шт Relay Module (SKU:DFR0017))

### Схема подключения ###
* GND pi - GND on shield
* 5V pi  - VIN on shield
* BCM 17 (physical 11) pi - D2 on shield (COM1-NO1) - Kate's gate
* BCM 27 (physical 13) pi - D7 on shield (COM2-NO2) - Stepa's gate
* BCM 22 (physical 15) pi - D8 on shield (COM3-NO3) - light in the garage
* BCM 23 (physical 16) pi - D10 on shield (COM4-NO4) - light on fence
* BCM 24 (physical 18) pi - switch outside for lamps in the garage. Выключатель подключен одник концом только к источнику питания 3.3 Вольт на Pi, другим концом к входу 24 (физический 18) pi и через резистор 300 Ом на землю Pi. Таким образом в разомкнутом состоянии на 24м пине 0В, а в сомкнутом напряжение 3.3 В. (Важно! Нельзя использовать сопротивление 10 кОм (как это сделано во многих примерах с Arduino),вместо 300 Ом тк это ведет к ложным срабатываниям выключателя в разомкнутом состоянии -  сопротивления 10 кОм оказалось сильно большое и на пин 24 шли наводки, приводящие к ложному срабатыванию).

### Автозапуск скрипта ###
* sudo vim /etc/rc.local
* Добавляем строчку
* su pi -c 'sudo node /home/pi/garage/app.js >> /home/pi/garage/garage.log  < /dev/null &'

### Список ПО необходимого для открытия файлов ###

* KiCad EDA http://kicad-pcb.org/ - для просмотра и редактирования схемы проекта schemme.sch и schemme.pro
* nodejs - на нем написан сам сервер и управление установка простая:
* curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
* sudo apt-get install -y nodejs
