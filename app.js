var http = require("http");
var fs = require("fs");
var url = require("url");
var socketio = require("socket.io");

var pinGate1 = 17; // (D2 on shield COM1-NO1) pin for Kate"s gate
var pinGate2 = 27; // (D7 on shield COM2-NO2) pin for my gate
var pinLamps = [22, 23, 10]; // 22 = lamps in garage, 23 = lamps on fence, 25 = lamps outside on the ground
var garage = 0;
var fence = 1;
var pinLampsState = [];
var on = 1;
var off = 0;
var pinSwitchGarage= 24; // pin for real switch in the garage (button) for lamps
var stateSwitch = 0; // 0 or 1 - last value of pinSwitchGarage. we use to determine when we press 

var cameraGarageURL = "http://admin:MF4c41d6S7WPvRSp9Ijl@192.168.3.81/image/jpeg.cgi";

// Convenient date format for logging
function getDateTime() {
  var date = new Date();
  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;
  var min  = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;
  var sec  = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;
  var day  = date.getDate();
  day = (day < 10 ? "0" : "") + day;
  return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;
}

function log( txt ) {
  var time = getDateTime();
  console.log(time + " " + txt);
}

function digitalRead(pin)
{
    var output = fs.readFileSync("/sys/class/gpio/gpio" + pin + "/value", "utf8");
    var values = output.split(":");
    var result = Number(values[values.length - 1]);

    //console.log(result);
    return result
}

function digitalWrite(pin, value) {
  fs.writeFileSync("/sys/class/gpio/gpio" + pin + "/value", String(value));
}

function pinMode(pin, mode) {
  try {
  // it"s ok to receive error here - it means we already started script before and file exists
    fs.writeFileSync("/sys/class/gpio/export", String(pin));
  } catch(err) {
    log(err);
  }
  try {
  // error here usually happens when run not under root
  // TODO learn and setup wiringPi to be able to run as pi user
    fs.writeFileSync("/sys/class/gpio/gpio" + pin + "/direction", String(mode)); // mode = out, in
  } catch(err) {
    log(err);
    log("OMG use sudo !!!!!!!!!!");
    log("sudo node app.js");
    process.exit(code=1)
  }
}

function delay(ms) {
  var start = new Date().getTime();
  while(1) {
    if ((new Date().getTime() - start) > ms) {
      break;
    }
  }
}

// Send file content to client
function sendFile( res, file) {
//  fs.readFile( __dirname + file, "utf-8", function(err, content) {
  fs.readFile( __dirname + file, function(err, content) {
    if (!err) {
      res.end( content );
    } else {
      log( "Error, reading file: " + err );
      res.end( "Error, reading file " + file + ". Check log" );
    }
  });
}

log( "Application started! Waiting for server to start." );
log( "Directory: " + __dirname );

// Loading the index file . html displayed to the client
var server = http.createServer(function(req, res) {
  var parsedURL = url.parse(req.url, true);
  var path = parsedURL.pathname;
  var query = parsedURL.query;
  if(path != "/camgarage.jpg") log("Ask for " + path);
  switch (path) {
    case "/":
    case "/index.html":
      res.writeHead(200, {"Content-Type": "text/html"});
      sendFile( res, "/index.html" );
      break;
    case "/style.css":
      res.writeHead(200, {"Content-Type": "text/css"});
      sendFile( res, path );
      break;
    case "/camgarage.jpg":
    // TODO add blank image and send it if code is not 200
      //log("request for camgarage.jpg!");
      http.get(cameraGarageURL, function(resget) {
        if (resget.statusCode == 200) {
          //log("RESPONCE CODE 200!");
          //var img = new Buffer(data, "base64");
          res.writeHead(200, {
            "Content-Type": "image/jpg",
          });
          //log("Header 200 sent");
          resget.on("data", function(chunk){
            //log("received chunk");
            res.write(chunk);
            //log("sent chunk");
          });
          //log("all chunks sent");
          resget.on("end", function(){
            res.end();
          });
        }
      });
      break;
    default:
      res.writeHead(404, {"Content-Type": "text/plain"});
      res.write("404 Not found");
      log ("No case for:" + path);
      break;
  }
});
// Loading socket.io
var io = socketio.listen(server);

// When a client connects, we note it in the console
io.sockets.on("connection", function (socket) {
  log("A client is connected!");

  socket.emit("broadcast", {  pinLampsState });

  socket.on("lamps", function(pin_num){
    log("lamps event:" + pin_num);
    pin = parseInt(pin_num);
    pinLampsState[pin] = 1 - pinLampsState[pin];
    digitalWrite( pinLamps[pin], pinLampsState[pin] );
    io.sockets.emit("broadcast", {  pinLampsState });
  });

  socket.on("message", function (message) {
    switch(message) {
      case "gate1":
        log("gate1 (Kate) button");
        digitalWrite( pinGate1, 1);
        delay(400);
        digitalWrite( pinGate1, 0);
        break;
      case "gate2":
        log("gate2 (Stepa) button");
        digitalWrite( pinGate2, 1 );
        delay(400);
        digitalWrite( pinGate2, 0);
        break;
      default:
        log("strange massage: ", message);
    }

    //log("A client is speaking to me! They’re saying: " + message);
  });
});


function loop() {
// Checking if some-one press real switch for Garage light (button) in the garage.
  if( stateSwitch != digitalRead(pinSwitchGarage) ) {
    log("Lamps switch in tha garage pressed!");
    stateSwitch = digitalRead(pinSwitchGarage);
    pinLampsState[garage] = 1 - pinLampsState[garage];
    digitalWrite( pinLamps[garage], pinLampsState[garage] );
  }
}

pinMode( pinGate1, "out" );
pinMode( pinGate2, "out" );

for (var i = 0; i < pinLamps.length; i++) {
  pinMode( pinLamps[i], "out" );
  digitalWrite( pinLamps[i], off );
  pinLampsState[i] = off;
}

pinMode( pinSwitchGarage, "in" );
digitalWrite( pinGate1, 0 );
digitalWrite( pinGate2, 0 );
stateSwitch = digitalRead(pinSwitchGarage);

const CronJob = require("cron").CronJob;
const jobOnFenceLight = new CronJob ("00 00 17 * * *", function() {
  log("Started Cron job to turn Fence light ON");
  pinLampsState[fence] = on;
  digitalWrite( pinLamps[fence], pinLampsState[fence] );
  io.sockets.emit("broadcast", {  pinLampsState });
});
jobOnFenceLight.start();

const jobOffAllLight = new CronJob ("00 30 23 * * *", function() {
  log("Started Cron job to turn All light OFF");
  pinLampsState[fence] = off;
  digitalWrite( pinLamps[fence], pinLampsState[fence] );
  pinLampsState[garage] = off;
  digitalWrite( pinLamps[garage], pinLampsState[garage] );
  io.sockets.emit("broadcast", {  pinLampsState });
});
jobOffAllLight.start();

server.on("error", function(err) {
  log(err);
  log("Some one is already listening on 80 port, find and kill it!");
  log("sudo netstat -nlp | grep 80");
  process.exit(code=1);
});

server.listen(80);

log("Server listening on port 80");

// Checking if some-one press real switch (button) in the garage.
setInterval( loop, 200);
